<?php

namespace App\Controller;

use App\Repository\HotelRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Dto\Response\Transformer\HotelStatsResponseDtoTransformer;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class HotelsController extends AbstractController
{
    public $startDate = null;

    public $endDate = null;

    protected $repository;

    private $serializer;

    public function __construct( HotelRepository $repository, SerializerInterface $serializer)
    {
        $this->repository = $repository;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/hotels/{id}/stats", name="hotel_overtime", methods={"POST","HEAD"})
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function stats($id, Request $request): Response
    {
        $requestBody = $this->transformJson($request);

        // Todo: use proper validation package
        $requiredParameters = [
            'start_date'    => 'Start Date is required',
            'end_date'     => 'End Date is required'
        ];
        foreach ($requiredParameters as $parameter => $errorMessage) {
            if (!isset($requestBody[$parameter])) {
                return $this->errorResponse($errorMessage);
            }
        }

        $this->startDate = $requestBody['start_date'];
        $this->endDate = $requestBody['end_date'];

        // Check group type
        $groupType = $this->getGroupType($this->startDate, $this->endDate);

        $data = $this->repository->getHotelStats($id, $this->startDate, $this->endDate, $groupType);

        $data = $this->serializer->serialize([
            'status' => 'success',
            'data' => $data
        ], JsonEncoder::FORMAT);
        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    private function getGroupType($startDate, $endDate)
    {
        $numberOfDays = (int) Carbon::parse( $startDate )->diffInDays( $endDate );

        if( $numberOfDays ){
            if ( $numberOfDays <= 29 ){
                return 'daily';
            } else if ( $numberOfDays <= 89 ){
                return 'weekly';
            }
            return 'monthly';
        }

        return false;
    }

    private function transformJson(Request $request)
    {
        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }
        return $parametersAsArray;
    }

    private function errorResponse(string $errorMessage): JsonResponse
    {
        $data = $this->serializer->serialize([
            'status' => 'error',
            'message' => $errorMessage
        ], JsonEncoder::FORMAT);
        return new JsonResponse($data, Response::HTTP_BAD_REQUEST, [], true);

    }
}
