<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Review;

class ReviewFixtures extends BaseFixture
{
    public function loadData(ObjectManager $manager): void
    {
        $this->createMany(Review::class, 10000, function(Review $review) 
        {
            $review->setComment( $this->faker->sentences(2, true) );
            $review->setScore( $this->faker->numberBetween(1, 10) );
            $review->setCreatedDate( $this->faker->dateTimeBetween('-2 years', '-1 seconds') );   
            $review->setHotel($this->getReference(Hotel::class.'_'.$this->faker->numberBetween(0, 9)));
        });

        $manager->flush();
    }
}
