<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use App\Entity\Hotel;

class HotelFixtures extends BaseFixture
{
    public function loadData(ObjectManager $manager): void 
    {
        $this->createMany(Hotel::class, 10, function(Hotel $hotel, $count) use ($manager) {            
            $hotel->setName($this->faker->company);
        });

        $manager->flush();
    }
}
