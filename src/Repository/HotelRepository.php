<?php

namespace App\Repository;

use App\Entity\Hotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @method Hotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hotel[]    findAll()
 * @method Hotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HotelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hotel::class);
    }

    /**
     * @Groups("hotel:stats")
     */
    public function getHotelStats($id, $startDate, $endDate, $groupBy)
    {
        switch ($groupBy) {
            case "week":
                $duration = 'WEEK(r.created_date) as week';
                $durationAlias = 'week';
                $dateGroup = 'calendar-week';
                break;
            case "monthly":
                $duration = 'MONTH(r.created_date) as month';
                $durationAlias = $dateGroup =  'month';
                break;
            default:
                $duration = 'DAY(r.created_date) as day';
                $durationAlias = $dateGroup = 'day';
        }

        return $this->createQueryBuilder('h')
            ->select("count(r.id) as review_count, avg(r.score) as average_score")
            ->addSelect($duration)
            ->addSelect("'".$dateGroup."' as date_group")
            ->andWhere('h.id = :id')
            ->andWhere('r.created_date between :startDate and :endDate')
            ->setParameter('id', $id)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate.' 23:59:59')
            ->leftJoin('h.reviews', 'r')
            ->groupBy($durationAlias)
            ->getQuery()
            ->getResult();
    }
}
