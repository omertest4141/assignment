<?php

namespace App\Tests\api;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Tests\ApiTester;
use Codeception\Util\HttpCode;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;

class HotelCest
{
    private Generator $faker;


    protected AbstractDatabaseTool $databaseTool;
    protected EntityManagerInterface $entityManager;

    public function _before(ApiTester $I)
    {
        $this->faker = Factory::create();
    }

    public function requestSuccessfull(ApiTester $I)
    {
        $data = [
            'start_date'   => '2021-01-01',
            'end_date'     => '2021-01-02'
        ];
        $response = $I->sendPost(
            'https://localhost:8000/hotels/1/stats',
            json_encode($data)
        );

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $response = json_decode($response, true);

        $I->assertEquals('success', $response['status']);
        $I->assertEquals(2, count($response['data']));
    }

    public function startdateMissingRequest(ApiTester $I)
    {
        $data = [
            'end_date'     => '2021-01-01'
        ];
        $response = $I->sendPost(
            'https://localhost:8000/hotels/1/stats',
            json_encode($data)
        );

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();

        $response = json_decode($response, true);
        $I->assertEquals('error', $response['status']);
        $I->assertEquals('Start Date is required', $response['message']);
    }

    public function enddateMissingRequest(ApiTester $I)
    {
        $data = [
            'start_date'     => '2021-01-01'
        ];
        $response = $I->sendPost(
            'https://localhost:8000/hotels/1/stats',
            json_encode($data)
        );

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();

        $response = json_decode($response, true);
        $I->assertEquals('error', $response['status']);
        $I->assertEquals('End Date is required', $response['message']);
    }

    public function checkDataFormatValidity(ApiTester $I)
    {
        $data = [
            'start_date'     => '2021-01-01',
            'end_date'     => '2021-01-02'
        ];
        $response = $I->sendPost(
            'https://localhost:8000/hotels/1/stats',
            json_encode($data)
        );

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $response = json_decode($response, true);

        foreach($response['data'] as $one){
            $I->assertArrayHasKey('review_count', $one);
            $I->assertArrayHasKey('average_score', $one);
            $I->assertEquals('day', $one['date_group']);
        }
    }

}