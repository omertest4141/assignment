# Instructions
Please follow the instructions below to setup the project.

1) Clone the repository.
2) Run "composer install"
3) Rename .env.sample to .env and configure.
4) Run "symfony server:start -d"
5) Run "symfony console doctrine:database:create"
6) Run "symfony console doctrine:migrations:migrate"
7) Run "symfony console doctrine:fixtures:load"

POST REQEUST:

ENDPOINT: https://localhost:8000/hotels/1/stats

PAYLOAD:
{
"start_date": "2022-01-01",
"end_date": "2022-01-10"
}

FUNCTIONAL TESTING:
1) For testing please rename the database to customeralliance_test.
2) Run "php vendor/bin/codecept run"

It was my first time using symphony, but i enjoyed the challenge.